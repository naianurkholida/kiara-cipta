@extends('component.layouts.master')

@section('content')
<div class="card">
	<div class="card-header">
		<h4>Settings</h4>
	</div>
	<div class="card-body">
		<form action="{{Route('setup.store_settings')}}" method="POST" id="form_menu" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Banner Terms</label>
						<select class="form-control" name="banner_terms">
							<option value="" selected="">- Select -</option>
							<?php foreach ($pages as $key => $item) { ?>
								<option value="{{ $item->key_page }}" <?php if($banner_terms == $item->key_page) { ?> selected <?php } ?> >
									{{ $item->judul_page }}
								</option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>About Us</label>
						<select class="form-control" name="about_us">
							<option value="" selected="">- Select -</option>
							<?php foreach ($pages as $key => $item) { ?>
								<option value="{{ $item->key_page }}" <?php if($about_us == $item->key_page) { ?> selected <?php } ?> >
									{{ $item->judul_page }}
								</option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Contacs Us</label>
						<select class="form-control" name="contacts_us">
							<option value="" selected="">- Select -</option>
							<?php foreach ($pages as $key => $item) { ?>
								<option value="{{ $item->key_page }}" <?php if($contacts_us == $item->key_page) { ?> selected <?php } ?> >
									{{ $item->judul_page }}
								</option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Faqs</label>
						<select class="form-control" name="faqs">
							<option value="" selected="">- Select -</option>
							<?php foreach ($category as $key => $item) { ?>
								<option value="{{ $item->id }}" <?php if($faqs == $item->id){ ?> selected <?php } ?> >{{ $item->category }}</option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Telephone</label>
						<input type="text" name="telepon" class="form-control" value="{{ $telepon }}">
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Phone</label>
						<input type="text" name="phone" class="form-control" value="{{ $phone }}">
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Website</label>
						<input type="text" name="website" class="form-control" value="{{ $website }}">
					</div>
				</div>

				<div class="col-md-3">
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" class="form-control" value="{{ $email }}">
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="alamat">{{ $alamat }}</textarea>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Services</label>
						<textarea class="form-control" name="services">{{ $services }}</textarea>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-lg-12"><br>
					<button type="submit" class="btn btn-info pull-right">Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection

@section('js')
<script src="{{asset('assets/admin/assets/js/demo5/pages/custom/user/add-user.js')}}" type="text/javascript"></script>	
<script type="text/javascript">

</script>
@endsection