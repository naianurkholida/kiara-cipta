@extends('fgn._partials.main')

@section('content')
<div class="our-blog section-spacing">
	<div class="container">
		<div class="row">
			<div class="col-xl-9 col-lg-8 col-12">
				<div class="post-wrapper blog-details">
					<div class="single-blog">
						<div class="image-box">
							<img src="{{ Helper::pathPosting() }}/{{ $data->image }}" alt="{{ $data->getPostingLanguage->judul }}">
							<div class="overlay"><a href="#" class="date">{{ Helper::tanggal_indonesia($data->created_at) }}</a></div>
						</div> <!-- /.image-box -->
						<div class="post-meta">
							<h5 class="title">{{ $data->getPostingLanguage->judul }}</h5>
							<p>{!! $data->getPostingLanguage->content  !!}</p>
						</div> <!-- /.post-meta -->
						<div class="share-option clearfix">
							<!-- <ul class="social-icon float-right">
								<li><i class="fa fa-share-alt" aria-hidden="true"></i> Share :</li>
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
							</ul> -->
						</div> <!-- /.share-option -->
					</div> <!-- /.single-blog -->
				</div> <!-- /.post-wrapper -->
				<!-- ==================== Related Post ================= -->
				<div class="inner-box">
					<div class="theme-title-one">
						<h2>@lang('language.relatedPost')</h2>
					</div> <!-- /.theme-title-one -->
					<div class="row">
						<div class="related-post-slider">
							<?php foreach (Helper::portofolio_related_post($data->id) as $key => $item) { ?>
								<div class="item">
									<div class="single-blog">
										<div class="image-box">
											<img src="{{ Helper::pathPosting() }}/{{ $item->posting->image }}" alt="{{ $item->posting->getPostingLanguage->judul }}">
											<div class="overlay"><a href="#" class="date">{{ Helper::tanggal_indonesia($item->posting->created_at) }}</a></div>
										</div> <!-- /.image-box -->
										<div class="post-meta">
											<h5 class="title"><a href="{{ url('portofolio') }}/{{ $item->posting->getPostingLanguage->seo }}">{{ $item->posting->getPostingLanguage->judul }}</a></h5>
											<a href="{{ url('portofolio') }}/{{ $item->posting->getPostingLanguage->seo }}" class="read-more">READ MORE</a>
										</div> <!-- /.post-meta -->
									</div> <!-- /.single-blog -->
								</div> <!-- /.col- -->
							<?php } ?>
						</div> <!-- /.related-product-slider -->
					</div> <!-- /.row -->
				</div> <!-- /.inner-box -->
			</div>
			<!-- ===================== Blog Sidebar ==================== -->
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8 col-12 blog-sidebar">
				<div class="sidebar-container sidebar-recent-post">
					<h5 class="title">@lang('language.recentPost')</h5>
					<ul>
						<?php foreach (Helper::portofolio_recent() as $key => $item) { ?>
							<li class="clearfix">
								<img src="{{ Helper::pathPosting() }}/{{ $item->image }}" alt="{{ $item->getPostingLanguage->judul }}" class="float-left">
								<div class="post float-left">
									<a href="{{ url('portofolio') }}/{{ $item->getPostingLanguage->seo }}">{{ $item->getPostingLanguage->judul }}</a>
									<div class="date">{{ Helper::tanggal_indonesia($item->created_at) }}</div>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div> <!-- /.sidebar-recent-post -->
			</div> <!-- /.col- -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</div>
@endsection