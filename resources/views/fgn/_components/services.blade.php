@extends('fgn._partials.main')

@section('content')
<br><br><br><br><br>
<br><br>
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<img src="{{ Helper::pathPosting() }}/{{ $data->image }}" alt="{{ $data->getPostingLanguage->judul }}" style="width: 100%;">
			<p style="margin: 0;">{{ Helper::tanggal_indonesia($data->created_at) }}</p>
			<h1 class="title">{{ $data->getPostingLanguage->judul }}</h1>
			<p>{!! $data->getPostingLanguage->content  !!}</p>
		</div>
		<div class="col-md-3">
			<h3>@lang('language.relatedPost')</h3> <br>
			<div class="related-product">
				<?php foreach (Helper::services_related_post($data->id) as $key => $item) { ?>
					<div class="item">
						<img src="{{ Helper::pathPosting() }}/{{ $item->posting->image }}" alt="{{ $item->posting->getPostingLanguage->judul }}" style="width: 100%;">				
						<h5 class="title"><a href="{{ url('our-clients') }}/{{ $item->posting->getPostingLanguage->seo }}">{{ $item->posting->getPostingLanguage->judul }}</a></h5>
						<a href="{{ url('our-clients') }}/{{ $item->posting->getPostingLanguage->seo }}" class="read-more">READ MORE</a>
					
					</div> <!-- /.col- -->
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<br><br><br><br>
@endsection