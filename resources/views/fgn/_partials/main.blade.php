<!DOCTYPE html>
<html lang="en">

<head>
  @include('fgn._partials.css')
	@yield('css')
</head>

<body id="home">
  <div class="body">
    @include('fgn._partials.header')
    
		@yield('content')

    @include('fgn._partials.footer')

  </div>

  @include('fgn._partials.js')
  @yield('js')
</body>

</html>