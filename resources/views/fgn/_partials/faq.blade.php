<!-- FAQ -->
<div class="container" id="faq" style="margin-top: 60px;margin-bottom: 50px;">
    <div class="about-inline">
        <h3>Pertanyaan Yang Umum Ditanyakan</h3>
        <!-- <h3>@lang('language.faqs')</h3> -->
    </div>
</div>

<!-- FAQ -->
<div class="service3 icon-box-square">
    <div class="container">
        <?php foreach(Helper::faqs() as $key => $item ){ ?>
            <button class="accordion">{{ $item->join->judul_page }}</button>
            <div class="panel">
            <p>{!! $item->join->konten_page !!}</p>
            </div>
        <?php } ?>
    </div>
</div>