<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>PT Fajar Gemintang Nusantara</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('cp-fgn/images/ic.png')}}" />

<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset('cp-fgn/css/bootstrap.min.css')}}" />

<!-- Icons -->
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous" />
<link rel="stylesheet" href="{{ asset('cp-fgn/css/ilmosys-icon.css')}}" />
<link rel="stylesheet" href="{{ asset('cp-fgn/js/vendors/swipebox/css/swipebox.min.css')}}" />

<!-- Theme CSS -->
<link rel="stylesheet" href="{{ asset('cp-fgn/js/vendors/slick/slick.css')}}" />
<link rel="stylesheet" href="{{ asset('cp-fgn/css/style.css')}}" />