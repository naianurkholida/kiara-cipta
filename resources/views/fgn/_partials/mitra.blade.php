<div class="container">
    <div class="clearfix space90"></div>
    <div class="about-inline">
        <h3>Mitra kami</h3>
    </div>
    <div class="flex-client">
    <?php foreach (Helper::our_supports() as $key => $item) { ?>
        <div class="item-client">
            <a href="{{ Helper::pathGallery() }}/{{ $item->image }}" target="blank">
                <img src="{{ Helper::pathGallery() }}/{{ $item->image }}" alt="Mitra Kami {{ $item->image }}">
            </a>
        </div>
    <?php } ?>
        <!-- <div class="item-client">
            <img src="{{asset('cp-fgn/images/clients/1.png')}}" alt="">
        </div>

        <div class="item-client">
            <img src="{{asset('cp-fgn/images/clients/2.png')}}" alt="">
        </div>
        <div class="item-client">
            <img src="{{asset('cp-fgn/images/clients/3.png')}}" alt="">
        </div>
        <div class="item-client">
            <img src="{{asset('cp-fgn/images/clients/4.png')}}" alt="">
        </div> -->
    </div>
</div>
