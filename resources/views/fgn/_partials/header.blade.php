<!-- HEADER -->
<header>
      <nav class="navbar-inverse navbar-lg navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <a href="#" class="navbar-brand brand"><img style="height: 50px;" src="{{ asset('cp-fgn/images/logo.png')}}" alt="logo" /></a>
          </div>

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right navbar-login">
              <li>
                <a href="tel:622287518335"><i class="ilmosys-headphone"></i> +62-22-87518335</a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <?php foreach (Helper::MenuFrontPage() as $key => $item) { ?>
                <li class="dropdown mm-menu">
                    <a class="page-scroll" href="{{ url($item->url) }}">{{ $item->getMenuFrontPageLanguage->judul_menu }}</a>
                  </li>
              <?php } ?>
              
              <!-- <li class="dropdown mm-menu">
                <a class="page-scroll" href="#agents">Tentang Kami</a>
              </li>

              <li class="dropdown mm-menu">
                <a class="page-scroll" href="produk.html">Produk</a>
              </li>

              <li class="dropdown mm-menu">
                <a class="page-scroll" href="#reviews">Testimoni</a>
              </li>

              <li class="dropdown mm-menu">
                <a class="page-scroll" href="#contact-info">Contact</a>
              </li> -->
            </ul>
          </div>
        </div>
      </nav>
    </header>
