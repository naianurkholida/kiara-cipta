<!-- About Us -->
<div id="agents" class="agent agent-dark">
    <div class="container">       
        <div class="about-inline">
            <h3>{{ Helper::about_us()->join->judul_page }}</h3>
        </div>
        <div class="row">
            <div class="col-lg-12 col-12">
                <p style="color: white !important; text-align: center;">
                    {!! Helper::about_us()->join->konten_page  !!}
                </p>
            </div>
            <!-- <div class="col-lg-5 col-12">
                <div class="quote-form">
                    <form class="theme-form-one" id="formInbox">
                        {{ csrf_field() }}
                        <div class="flex-input">
                            <input class="form-control" style="margin-bottom: 10px;" type="text" name="nama" placeholder="Name *">
                            <input class="form-control" style="margin-bottom: 10px;" type="text" name="phone" placeholder="Phone *">
                            <input class="form-control" style="margin-bottom: 10px;" type="email" name="email" placeholder="Email *">
                            
                                <select class="form-control" style="margin-bottom: 10px;" id="exampleSelect1" name="services">
                                    <option selected="" value="">Choose Services?</option>
                                    <?php foreach (Helper::services() as $key => $item) { ?>
                                        <option value="{{ $item }}">{{ $item }}</option>
                                    <?php } ?>
                                </select>
                            
                            <textarea class="form-control" style="margin-bottom: 10px;" placeholder="Message" name="message"></textarea>
                        </div>
                        <button id="sendInbox" class="btn btn-primary btn-block theme-button-one">SEND</button>
                    </form>
                </div> 
            </div> -->
        </div>
        <div class="space100"></div>
    </div>
</div>