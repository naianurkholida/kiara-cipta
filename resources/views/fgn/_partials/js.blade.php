
  <!-- Javascript =============================-->
  <script src="{{ asset('cp-fgn/js/jquery.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/slick/slick.min.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/jquery.easing.min.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/stellar.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/isotope/isotope.pkgd.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/swipebox/js/jquery.swipebox.min.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/main.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/mc/jquery.ketchup.all.min.js')}}"></script>
  <script src="{{ asset('cp-fgn/js/vendors/mc/main.js')}}"></script>

  <script>
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }
</script>