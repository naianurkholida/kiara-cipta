<!-- Contact -->
<div id="contact-info" class="bg-white" style="padding-top: 50px;padding-bottom: 100px;">
    <div class="container">
        <div class="about-inline">
            <h3>Ada pertanyaan ? Hubungi kami</h3>
            <p style="max-width: 100%;">
                Jika anda ingin menghubungi kami, maka berikut beberapa informasi kontak dari kami yang dapat dihubungi.
            </p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="c-info">
                    <i class="ilmosys-phone"></i>
                    <h5>Hubungi Kami</h5>
                    <p>(022) 87518335</p>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="c-info">
                    <i class="ilmosys-link"></i>
                    <h5>Website</h5>
                    <p><a href="#" class="" style="font-size: 11px;">www.fajargemintangnusantara.com</a></p>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="c-info">
                    <i class="ilmosys-map"></i>
                    <h5>Alamat</h5>
                    <p>Jl Sawah Kurung V No. 14, Kota Bandung</p>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <div class="c-info">
                    <i class="ilmosys-envelope"></i>
                    <h5>Email</h5>
                    <p><a href="#" class="#" style="font-size:11px;"> fajargemintangnusantara@gmail.com</a></p>
                </div>
            </div>
        </div>
    </div>
</div>