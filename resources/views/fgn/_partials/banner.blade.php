<!-- BANNER -->
<div class="intro intro15">
    <div class="container">
        <div class="row center-content">
            <div class="col-md-5">
                <h3>
                    {{ Helper::slider()->join->judul }}
                </h3>
                <p>
                {{ Helper::slider()->join->deskripsi }}
                </p>
                <div class="space40"></div>

                <!-- <form class="intro-newsletter intro-newsletter-full intro10-nl" action="#" id="invite" method="POST">
                    <div class="row">
                        <div class="col-md-8 col-sm-7 col-xs-8 no-padding">
                            <input class="" placeholder="Kota, Alamat" name="city" id="#" />
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-4 no-padding">
                            <button class="btn btn-primary btn-block" type="submit">
                                Cari
                            </button>
                        </div>
                    </div>
                </form> -->
                <div class="space40"></div>
            </div>

            <div class="col-md-7">
                <div class="hl-container pull-right">
                    <img src="{{ Helper::pathSlider() }}/{{ Helper::slider()->image }}" class="hl-image" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>