 <!-- FOOTER -->
 <footer class="footer2" id="footer2">
     <div class="container">
         <div class="row">
             <div class="col-md-3 footerP">
                 <h5>— Tentang Kami</h5>

                 <p>
                 </p>
                 <div class="footer-social">
                     <a href="#" class="fa fa-facebook"></a>
                     <a href="#" class="fa fa-twitter"></a>
                     <a href="#" class="fa fa-dribbble"></a>
                     <a href="#" class="fa fa-linkedin"></a>
                     <a href="#" class="fa fa-instagram"></a>
                 </div>
             </div>

             <div class="col-md-3 footerP">
                 <h5>— RELATED LINKS</h5>
                 <ul>
                     <li>
                         <a href="/about-us">
                             <p>About Us</p>
                         </a>
                     </li>
                     <li>
                         <a href="/services">
                             <p>Product</p>
                         </a>
                     </li>

                 </ul>
             </div>

             <div class="col-md-3 footerP">
                 <h5>— RELATED LINKS</h5>
                 <ul>
                     <li>
                         <a href="/portofolio">
                             <p>Portofolio</p>
                         </a>
                     </li>
                     <li>
                         <a href="/contact">
                             <p>Contact Us</p>
                         </a>
                     </li>
                 </ul>
             </div>


         </div>
     </div>
 </footer>

 <!-- Copyright -->
 <div class="footer-copy">
     <div class="container">&copy; 2022. PT FAJAR GEMINTANG NUSANTARA. All rights reserved.</div>
 </div>

 <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="modalContactForm"
     aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header text-center">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                 </button>
                 <div class="cf2-wrap">
                     <h2>Make an Inquiry Now!</h2>

                     <div class="clearfix space40"></div>
                     <form class="positioned" name="sentMessage" id="contactForm" action="php/contact.php"
                         method="post">
                         <div class="row">
                             <div class="col-md-6 col-sm-6">
                                 <input name="senderName" id="senderName" placeholder="Your Name" required
                                     type="text" />
                             </div>
                             <div class="col-md-6 col-sm-6">
                                 <input name="senderEmail" id="senderEmail" placeholder="Email Address" required
                                     type="email" />
                             </div>
                         </div>

                         <input name="address" id="address" placeholder="Address" required type="text" />

                         <input placeholder="Zip Code" required type="number" />

                         <input placeholder="Mobile No." required type="number" />

                         <button type="submit" class="btn btn-primary btn-ico">
                             Send Message<i class="fa fa-paper-plane-o"></i>
                         </button>
                     </form>
                     <div id="sendingMessage" class="statusMessage">
                         <p>
                             <i class="fa fa-spin fa-cog"></i> Sending your message.
                             Please wait...
                         </p>
                     </div>
                     <div id="successMessage" class="successmessage">
                         <p>
                             <span class="success-ico"></span> Thanks for sending your
                             message! We'll get back to you shortly.
                         </p>
                     </div>
                     <div id="failureMessage" class="errormessage">
                         <p>
                             <span class="error-ico"></span> There was a problem sending
                             your message. Please try again.
                         </p>
                     </div>
                     <div id="incompleteMessage" class="statusMessage">
                         <p>
                             <i class="fa fa-warning"></i> Please complete all the fields
                             in the form before sending.
                         </p>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
