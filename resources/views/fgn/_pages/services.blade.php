@extends('fgn._partials.main')

@section('content')
<div class="bg-pattern">
    <!-- OUR Produk -->
    <div class="container">
        <div class="clearfix space90"></div>
        <div class="about-inline">
            <h3>Produk & Layanan</h3>
        </div>
        <div class="flex-client">
            <?php foreach (Helper::services_view() as $key => $item) { ?>
                <div class="item-client">
                    <img src="{{ Helper::pathPosting() }}/{{ $item->image }}" alt="">
                    <h3 style="margin-top: 20px;margin-bottom: 20px;"><a href="{{url('services')}}/{{ $item->getPostingLanguage->seo }}">{{ $item->getPostingLanguage->judul }}</a></h3>
                </div>
            <?php } ?>
            <!-- <div class="item-client">
                <img src="{{ asset('cp-fgn/images/clients/2.png')}}" alt="">
            </div>
            <div class="item-client">
                <img src="{{ asset('cp-fgn/images/clients/2.png')}}" alt="">
            </div>
            <div class="item-client">
                <img src="{{ asset('cp-fgn/images/clients/3.png')}}" alt="">
            </div>
            <div class="item-client">
                <img src="{{ asset('cp-fgn/images/clients/4.png')}}" alt="">
            </div> -->
        </div>
    </div>


    <div class="space100"></div>
</div>
@endsection