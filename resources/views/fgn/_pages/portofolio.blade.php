@extends('fgn._partials.main')

@section('content')
<br><br><br><br><br><br>
<div class="our-blog latest-news section-spacing">
	<div class="container">
		<div class="theme-title-one">
			<h2>PORTOFOLIO</h2>
			<p>PT. FAJAR GEMINTANG NUSANTARA</p>
		</div> <!-- /.theme-title-one -->
		<br>
		<div class="flex-portofolio">
			<?php foreach(Helper::portofolio() as $key => $item){ ?>
				<div class="item-porto">
					<div class="image-box">
						<img src="{{ Helper::pathPosting() }}/{{ $item->image }}" alt="{{ $item->getPostingLanguage->judul }}" style="height: 300px;">							
					</div> <!-- /.image-box -->
					<p style="margin-bottom: 0;" class="date">{{ Helper::tanggal_indonesia($item->created_at) }}</p>
					<div class="desc-porto">
						<!-- <h4 class="title"><a href="{{ url('portofolio') }}/{{ $item->getPostingLanguage->seo }}">{{ $item->getPostingLanguage->judul }}</a></h4> -->
						<h4 class="title">{{ $item->getPostingLanguage->judul }}</h4>
						<!-- <a href="{{ url('portofolio') }}/{{ $item->getPostingLanguage->seo }}" class="read-more">READ MORE</a> -->
					</div> <!-- /.post-meta -->
				</div> <!-- /.single-blog -->
			<?php } ?>
		</div> 
	</div> <!-- /.container -->
</div> 
<br><br><br><br>
@endsection