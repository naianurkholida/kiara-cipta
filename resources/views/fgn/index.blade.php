@extends('fgn._partials.main')

@section('content')
<!-- BANNER -->
@include('fgn._partials.banner')

<!-- Client -->
@include('fgn._partials.client')

<!-- About Us -->
@include('fgn._partials.about-us')

<!-- Mitra Kami -->
@include('fgn._partials.mitra')



<!-- FAQ -->
@include('fgn._partials.faq')


<!-- PARALLAX -->
<section class="parallax-content2 parallax2 text-center" data-stellar-background-ratio="0.4">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 style="text-align: center">Cara terbaik untuk membeli Kebutuhan Pokok.</h4>
            </div>
            <!-- <div class="row">
                <div class="col-md-4">
                    <a href="{{ url('about-us') }}" class="btn btn-lg btn-primary pull-left"> <i class="icon-arrow-right"></i></a>
                </div>
            </div> -->
        </div>
    </div>
</section>

<!-- Reviews -->
<div id="reviews" class="bg-light">
    <div class="container">
        <div class="about-inline" style="margin-top: 50px;">
            <h3>Ulasan Klien Kami</h3>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="quote3">
                    @foreach(Helper::reviews() as $item)
                    <div>
                        <!-- <img src="images/reviews/testi-1.png" alt="" /> -->
                        <p>
                            {!! $item->join->konten_page  !!}
                        </p>
                        <span class="author"><b>{{ $item->join->judul_page }}</b></span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="space100"></div>
    </div>
</div>

<!-- GOOGLE MAP -->
<div class="google-map">
    <div class="container-fluid no-padding">
        <div id="map"></div>
    </div>
</div>

<div class="clear"></div>

<!-- CONTACT -->
@include('fgn._partials.contact')

@endsection