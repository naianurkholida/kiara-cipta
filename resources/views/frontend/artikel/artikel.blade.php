@extends('frontend.layouts.master')

@section('content')
    <!-- breadcrumb section start -->
    <section class="breadcrumb-section pt-180 pb-180 bg_img"
        data-background="{{ asset('assets/frontend/images/bg/banner-artikel.png') }}" data-overlay="dark" data-opacity="3">
        <div class="container">

            <div class="row">
                <div class="col-xl-8 d-flex pr-0">
                    <div class="breadcrumb-text">
                        <h2 class="breadcrumb-text__title">
                            ARTIKEL TERBARU
                        </h2>
                        <ul class="breadcrumb-text__nav">
                            <li><a href="{{ route('page.home') }}">Home</a></li>
                            <li>-</li>
                            <li>Artikel Terbaru</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb section end -->

    <!-- news area start -->
    <div class="blog__area blog__area--2 pt-125 pb-125">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12">
                    @foreach (Helper::get_article() as $article)
                        <article class="blog__box blog__box--3 blog__box--image mb-40">
                            <div class="thumb">
                                <a href="{{ route('page.detail-showcase', $article->id)}}">
                                    <img src="{{ asset('assets/admin/assets/media/posting/'.$article->image) }}"
                                        alt="blog image">
                                </a>
                            </div>
                            <div class="content pt-50">
                                <div class="cat">
                                    <span>{{ $article->category->category }}</span>
                                </div>
                                <h3 class="title">
                                    <a
                                        href="{{ route('page.detail-showcase', $article->id) }}">{{ $article->getPostingLanguage->judul }}</a>
                                </h3>
                                <div class="meta mt-20 mb-20">
                                    <span><a href="#0"><i class="far fa-calendar-alt"></i>
                                            {{ \Carbon\Carbon::parse($article->created_at)->locale('id')->isoFormat('dddd, D MMMM YYYY') }}</a></span>
                                <div class="post-text mb-35">
                            </div>
                                </div>
                                <div class="post-bottom mt-30">
                                    <div class="authore">
                                        <img src="{{ asset('assets/frontend/images/news/news-list-authore.png') }}"
                                            alt="">
                                        <span>by Hetmayar</span>
                                    </div>
                                    <a href="{{ route('page.detail-showcase', $article->id) }}" class="inline-btn"><span class="icon"><i
                                                class="fal fa-arrow-right"></i></span> Selengkapnya</a>
                                </div>
                            </div>
                        </article>
                    @endforeach
                    <div class="blog__pagination mt-40">
                        <ul>
                            <li><a href="#0"><i class="fas fa-angle-double-left"></i></a></li>
                            <li><a href="#0">01</a></li>
                            <li class="active"><a href="#0">02</a></li>
                            <li><a href="#0">03</a></li>
                            <li><a href="#0"><i class="fas fa-ellipsis-h"></i></a></li>
                            <li><a href="#0"><i class="fas fa-angle-double-right"></i></a></li>
                        </ul>
                    </div>
                </div>



                <div class="col-xl-4 col-lg-12">
                    <div class="sidebar-wrap">
                        <div class="widget sidebar grey-bg mb-40">
                            <h4 class="sidebar__title mb-30">
                                <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-3.png') }}"
                                        class="mr-5" alt=""></span>
                                Search Objects
                            </h4>
                            <form class="sidebar-search-form">
                                <input type="text" placeholder="Search your keyword...">
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                        <div class="widget sidebar grey-bg mb-40">
                            <h4 class="sidebar__title mb-30">
                                <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-3.png') }}"
                                        class="mr-5" alt=""></span>
                                Categories
                            </h4>
                            <ul class="sidebar__list">
                                <li>
                                    <a href="">Banner Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">Billboard Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">Business Card <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">Poster Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">Poster Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="">T-Shirt Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="widget sidebar grey-bg mb-40">
                            <h4 class="sidebar__title mb-30">
                                <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-3.png') }}"
                                        class="mr-5" alt=""></span>
                                Popular Tags
                            </h4>
                            <div class="tag">
                                <a href="#0" class="site-btn">Popular</a>
                                <a href="#0" class="site-btn">desgin</a>
                                <a href="#0" class="site-btn">ux</a>
                                <a href="#0" class="site-btn">usability</a>
                                <a href="#0" class="site-btn">develop</a>
                                <a href="#0" class="site-btn">icon</a>
                                <a href="#0" class="site-btn">business</a>
                                <a href="#0" class="site-btn">consult</a>
                                <a href="#0" class="site-btn">kit</a>
                                <a href="#0" class="site-btn">keyboard</a>
                                <a href="#0" class="site-btn">mouse</a>
                                <a href="#0" class="site-btn">tech</a>
                            </div>
                        </div>
                        <div class="widget sidebar grey-bg ad__widget">
                            <img src="{{ asset('assets/frontend/images/bg/tmbh-banner.png') }}" alt="">
                            <div class="ad-text">
                                <h3><span>350x600</span>Add Banner</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- news area end -->
@endsection
