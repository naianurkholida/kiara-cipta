@extends('frontend.layouts.master')

@section('content')
   <!-- breadcrumb section start -->
   <section class="breadcrumb-section pt-180 pb-180 bg_img" data-background="{{asset('assets/frontend/images/bg/banner-artikel.png')}}"
        data-overlay="dark" data-opacity="3">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 d-flex pr-0">
                    <div class="breadcrumb-text">
                        <h2 class="breadcrumb-text__title">
                        DETAIL ARTIKEL
                        </h2>
                        <ul class="breadcrumb-text__nav">
                            <li><a href="{{route('page.home')}}">Home</a></li>
                            <li>-</li>
                            <li>Artikel Terbaru</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb section end -->

    <!-- news area start -->
    <div class="blog__area blog__area--2 pt-125 pb-125">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12">
                <article class="blog__box blog__box--3 blog__box--details">
                        <div class="content pt-50">
                            <div class="cat mb-20">
                                <span>Businese</span>
                            </div>
                            <h3 class="title">
                            {{ $showcase->getPostingLanguage->judul }}
                            </h3>
                            <div class="blog-inner-img mt-40 mb-40">
                                <div class="inner-content">
                                    <img src="{{asset('assets/admin/assets/media/posting/'.$showcase->image)}}" alt="blog image">
                                </div>
                            </div>
                            <div class="meta mt-20 mb-20">
                                <span><i class="far fa-user"></i> by Piklo D. Sindom </span>
                                <span><a href="#0"><i class="far fa-calendar-alt"></i> {{ \Carbon\Carbon::parse($showcase->created_at)->locale('id')->isoFormat('dddd, D MMMM YYYY') }}</a></span>
                                <span><a href="#0"><i class="far fa-comments"></i> 23 Comments</a></span>
                            </div>
                            <div class="post-text mb-20">
                                <p>{!! $showcase->getPostingLanguage->content !!}</p>
                            </div>
                            
                            <div class="inner-content mb-35">
                                <h4 class="mb-25"></h4>
                                <p></p>
                            </div>
                            {{-- <div class="row">
                                <div class="col-12">
                                    <div class="navigation-border pt-35 mt-40"></div>
                                </div>
                            </div> --}}
                            <p>Dengan memperhatikan tips-tips ini, Anda dapat meningkatkan kemungkinan mendapatkan hasil cetakan yang berkualitas tinggi sesuai dengan desain yang diinginkan.</p>
                            <blockquote>
                                <p>Kualitas cetakan adalah cerminan dari komitmen bisnis untuk memberikan yang terbaik. 
                                Bisnis printing yang unggul bermula dari standar kualitas yang tinggi.</p>
                                <div class="quote-icon">
                                    <img src="{{asset('assets/frontend/images/icons/quote-icon.png')}}" alt="">
                                </div>
                            </blockquote>
                           
                            
                            <div class="row mt-40">
                                <div class="col-xl-7 col-lg-7 col-md-7">
                                    <div class="blog-post-tag">
                                        <span>Releted Tags</span>
                                        <a href="#0">DESIGN</a>
                                        <a href="#0">PRINTING</a>
                                        <a href="#0">WARNA</a>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5">
                                    <div class="blog-share-icon text-left text-md-right">
                                        <span>Share: </span>
                                        <a href="#0"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#0"><i class="fab fa-twitter"></i></a>
                                        <a href="#0"><i class="fab fa-instagram"></i></a>
                                        <a href="#0"><i class="fab fa-google-plus-g"></i></a>
                                        <a href="#0"><i class="fab fa-vimeo-v"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="navigation-border pt-50 mt-50"></div>
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5">
                                    <div class="bakix-navigation b-next-post text-left mb-30">
                                        <span><a href="#0">Prev Post</a></span>
                                        <h4><a href="#0">Tips on Minimalist</a></h4>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-lg-2 col-md-2 my-auto">
                                </div>
                                <div class="col-xl-5 col-lg-5 col-md-5">
                                    <div class="bakix-navigation b-next-post text-left text-md-right mb-30">
                                        <span><a href="#0">Next Post</a></span>
                                        <h4><a href="#0">Less Is More</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                
                

                <div class="col-xl-4 col-lg-12">
                    <div class="sidebar-wrap">
                        <div class="widget sidebar grey-bg mb-40">
                            <h4 class="sidebar__title mb-30">
                                <span><img src="{{asset('assets/frontend/images/shape/heading-shape-3.png')}}" class="mr-5" alt=""></span>
                                Search Objects
                            </h4>
                            <form class="sidebar-search-form">
                                <input type="text" placeholder="Search your keyword...">
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                        <div class="widget sidebar grey-bg mb-40">
                            <h4 class="sidebar__title mb-30">
                                <span><img src="{{asset('assets/frontend/images/shape/heading-shape-3.png')}}" class="mr-5" alt=""></span>
                                Categories
                            </h4>
                            <ul class="sidebar__list">
                                <li>
                                    <a href="service-details.html">Banner Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="service-details.html">Billboard Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="service-details.html">Business Card <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="service-details.html">Poster Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="service-details.html">Poster Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="service-details.html">T-Shirt Printing <span class="icon">
                                            <span class="icon"><i class="far fa-arrow-right"></i></span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="widget sidebar grey-bg mb-40">
                            <h4 class="sidebar__title mb-30">
                                <span><img src="{{asset('assets/frontend/images/shape/heading-shape-3.png')}}" class="mr-5" alt=""></span>
                                Popular Tags
                            </h4>
                            <div class="tag">
                                <a href="#0" class="site-btn">Popular</a>
                                <a href="#0" class="site-btn">desgin</a>
                                <a href="#0" class="site-btn">ux</a>
                                <a href="#0" class="site-btn">usability</a>
                                <a href="#0" class="site-btn">develop</a>
                                <a href="#0" class="site-btn">icon</a>
                                <a href="#0" class="site-btn">business</a>
                                <a href="#0" class="site-btn">consult</a>
                                <a href="#0" class="site-btn">kit</a>
                                <a href="#0" class="site-btn">keyboard</a>
                                <a href="#0" class="site-btn">mouse</a>
                                <a href="#0" class="site-btn">tech</a>
                            </div>
                        </div>
                        <div class="widget sidebar grey-bg ad__widget">
                            <img src="{{asset('assets/frontend/images/bg/tmbh-banner.png')}}" alt="">
                            <div class="ad-text">
                                <h3><span>350x600</span>Add Banner</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- news area end -->
@endsection