@extends('frontend.layouts.master')
@section('content')
    @include('frontend.component.banner.banner')
    @include('frontend.component.feature.feature')
    @include('frontend.component.about.about')
    @include('frontend.component.project.portofolio')
    @include('frontend.component.service.service')
    @include('frontend.component.client.client')
    @include('frontend.component.showcase.showcase')
    
    <!-- cta section start -->
    <section class="cta-area theme-bg pt-105 pb-115">
        <div class="container">
            <div class="">
                <div class="col-xl-8">
                    <div class="section-header">
                        <h2 class="section-title section-title__white">Butuh Solusi Desain <br>
                            untuk Brand Anda?</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cta section end -->
    @include('frontend.component.contact.contact')


@endsection
