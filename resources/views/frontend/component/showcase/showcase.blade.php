    <!-- artikel section start -->
    <section class="news-area grey-bg pt-120 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-7">
                    <div class="section-header mb-80">
                        <h2 class="section-title">Artikel Terbaru</h2>
                    </div>
                </div>
                <div class="col-xl-5 text-right news-right">
                    <a href="{{ route('page.artikel') }}" class="inline-btn"><span class="icon"><i
                                class="far fa-arrow-right"></i></span>
                        blog lainnya</a>
                </div>
            </div>
            <div class="row mt-none-30">
            @foreach (Helper::get_article_dashboard() as $showcase )
                <div class="col-xl-4 col-lg-6 mt-30">
                    <article class="post-box">
                        <div class="post-box__thumb">
                            <img src="{{ asset('assets/admin/assets/media/posting/'.$showcase->image) }}" alt="">
                            <span class="post-box__cat">Artikel</span>
                        </div>
                        <div class="post-box__content text-center">
                            <a href="{{ route('page.detail-showcase', $showcase->id) }}" class="date-author">By Admin / {{ \Carbon\Carbon::parse($showcase->created_at)->locale('id')->isoFormat('dddd, D MMMM YYYY') }}</a>
                            <h4 class="post-box__title">
                                <a href="{{ route('page.detail-showcase', $showcase->id) }}">{!! $showcase->getPostingLanguage->judul !!}</a>
                            </h4>
                            <a href="{{ route('page.detail-showcase', $showcase->id) }}" class="inline-btn">
                                <span class="icon"><i class="far fa-arrow-right"></i></span>
                                Lanjutkan membaca
                            </a>
                        </div>
                    </article>
                </div>
            @endforeach
            </div>
        </div>
    </section>
    <!-- artikel section end -->