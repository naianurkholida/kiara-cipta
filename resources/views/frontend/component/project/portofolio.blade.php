    <!-- project section start -->
    <div class="project-area" id="portofolio">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center">
                    <div class="section-header mb-75">
                        <h2 class="section-title">
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-1.png') }}"
                                    class="mr-5" alt=""></span>
                            Portofolio
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-2.png') }}"
                                    class="mr-5" alt=""></span>
                        </h2>

                    </div>
                </div>
            </div>
            <div class="row project-row mt-none-30">
            @php $first = Helper::portofolio_gallery_first() @endphp
                <div class="col-xl-5 col-lg-6 col-md-6 d-flex align-self-stretch mt-30">
                    <div class="project-item">
                        <div class="project-item__thumb project-item__thumb--big">
                            <img src="{{ asset('assets/admin/assets/media/posting/'.$first->image) }}" alt="">
                        </div>
                        <div class="project-item__hover" data-overlay="dark" data-opacity="9">
                            <div class="project-item__content">
                                <h4 class="project-item__title">{{ $first->getPostingLanguage->judul }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 mt-30">
                    <div class="row mt-none-30">
                        @foreach (Helper::portofolio_gallery() as $gallery)
                            <div class="col-xl-6 col-md-6 col-lg-6 mt-30">
                                <div class="project-item">
                                    <div class="project-item__thumb">
                                        <img src="{{ asset('assets/admin/assets/media/posting/'.$gallery->image) }}"
                                            alt="">
                                    </div>
                                    <div class="project-item__hover" data-overlay="dark" data-opacity="9">
                                        <div class="project-item__content">
                                            <h4 class="project-item__title">{{ $gallery->getPostingLanguage->judul }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- project section end -->
