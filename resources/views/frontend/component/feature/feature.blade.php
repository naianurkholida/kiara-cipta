    <section class="feature-area pt-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center">
                    <div class="section-header mb-50">
                        <h2 class="section-title">
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-1.png') }}" class="mr-5"
                                    alt=""></span>
                            Fitur Kami
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-2.png') }}" class="mr-5"
                                    alt=""></span>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach (Helper::fitur_kami() as $post)
                    <div class="col-xl-3 col-lg-6 col-md-6 mt-30">
                    <div class="feature-item">
                            <div class="feature-item__icon feature-item__icon--1">
                                <img src="{{asset('assets/admin/assets/media/posting/'.$post->image)}}" alt="">
                            </div>
                            <div class="feature-item__content">
                                <h4 class="feature-item__title">{{ $post->getPostingLanguage->judul }}</h4>
                                <p>{!! $post->getPostingLanguage->content !!}</p>
                            </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- feature section end -->
