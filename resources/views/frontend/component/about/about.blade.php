    <!-- about section start -->
    <section class="about-area pt-130 pb-130" id="about">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-6 pr-0">
                    <div class="about__bg" data-tilt data-tilt-perspective="3000">
                        <img src="{{ asset('assets/frontend/images/bg/about-bg-1.png') }}" alt="">
                    </div>
                </div>
                <div class="col-xl-6 pl-80">
                    <div class="section-header mb-40">
                        <h2 class="section-title mb-35" >Tentang Kami </h2>
                        <p>Kiara Cipta berfokus pada bidang percetakan,
                            laser cutting akrilik dan advertising. Kami telah bekerja sama dengan intansi
                            / perusahaan besar dalam menggunakan jasa kami seperti sekolah, puskesmas, rumah sakit, yayasan
                            sosial, perkantoran swasta, dan lainnya. </p>
                    </div>
                    <div class="row mt-none-40">
                        <div class="col-xl-6 mt-40">
                            <div class="ab__box">
                                <div class="ab__box--head">
                                    <div class="icon">
                                        <img src="{{ asset('assets/frontend/images/icons/layanan-cepat.svg') }}"
                                            alt="">
                                    </div>
                                    <span class="title">Layanan cepat <br> dan berkualitas</span>
                                </div>
                                <p>Membuat produk yang berkualitas dan berinovasi baru sesuai dengan perkembangan zaman
                                </p>
                            </div>
                        </div>
                        <div class="col-xl-6 mt-40">
                            <div class="ab__box">
                                <div class="ab__box--head">
                                    <div class="icon icon__2">
                                        <img src="{{ asset('assets/frontend/images/icons/yang pasti.svg') }}"
                                            alt="">
                                    </div>
                                    <span class="title">Dapatkan yang <br>Paling Pasti</span>
                                </div>
                                <p>Pelayanan terbaik merupakan prioritas utama kami dalam menjalin hubungan dengan konsumen.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- about section end -->