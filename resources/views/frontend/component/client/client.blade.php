    <!-- client section start -->
    <div class="brand-section pt-125 pb-120" id="client">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center">
                    <div class="section-header mb-75">
                        <h2 class="section-title">
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-1.png') }}"
                                    class="mr-5" alt=""></span>
                            Client
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-2.png') }}"
                                    class="mr-5" alt=""></span>
                        </h2>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="brand-carousel owl-carousel">
                    @foreach (Helper::client() as $client)
                        
                        <div class="brand-carousel__item">
                            <img class="default" src="{{asset('assets/admin/assets/media/posting/'.$client->image)}}"
                                alt="">
                            <img class="hover" src="{{asset('assets/admin/assets/media/posting/'.$client->image)}}" alt="">
                        </div>
                    @endforeach
    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- client section end -->