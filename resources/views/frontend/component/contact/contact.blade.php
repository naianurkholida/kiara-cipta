    <!-- contact section start -->
    <section class="gta-area pt-125 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div id="contact-map">
                        <div class="map">
                            <iframe frameborder="0" scrolling="no" marginheight="0"
                                marginwidth="0" class="iframe-map"
                                src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Jl.%20Babakan%20Sari%20No.135,%20Babakan%20Sari,%20Kec.%20Kiaracondong,%20Kota%20Bandung,%20Jawa%20Barat%2040283+(My%20Business%20Name)&amp;t=&amp;z=19&amp;ie=UTF8&amp;iwloc=B&amp;output=embed">
                                <a href="https://www.maps.ie/population/">Kiara Cipta Printing</a>
                            </iframe>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 pl-70">
                    <div class="section-header mb-50">
                        <h2 class="section-title">Kontak Kami</h2>
                    </div>
                    <div class="contact-form">
                        <div class="container-contact d-flex mt-25">
                            <img src="{{ asset('assets/frontend/images/icons/email.svg') }}" alt=""
                                class="icons">
                            <div class="title-contact"><h5> Email</h5></div>
                            <div class="contact-text border w-100 p-4 d-flex justify-content-start align-items-center"
                                style="border-radius:40px;">
                                <h4 class="m-0">order@gmail.com</h4>
                            </div>
                        </div>
                        <div class="container-contact d-flex mt-25">
                            <img src="{{ asset('assets/frontend/images/icons/instagram.svg') }}" alt=""
                                class="icons">
                            <div class="title-contact"><h5>Instagram</h5></div>
                            <div class="contact-text border w-100 p-4 d-flex justify-content-start align-items-center"
                                style="border-radius:40px;">
                                <h4>kiaraciptaproduction</h4>
                            </div>
                        </div>
                        <div class="container-contact d-flex mt-25">
                            <img src="{{ asset('assets/frontend/images/icons/facebook.svg') }}" alt=""
                                class="icons">
                            <div class="title-contact"><h5>Facebook</h5></div>
                            <div class="contact-text border w-100 p-4 d-flex justify-content-start align-items-center"
                                style="border-radius:40px;">
                                <h4>kiaraciptaproduction</h4>
                            </div>
                        </div>
                        <div class="container-contact d-flex mt-25">
                            <img src="{{ asset('assets/frontend/images/icons/telepon.svg') }}" alt=""
                                class="icons">
                            <div class="title-contact"><h5>No Telepon</h5></div>
                            <div class="contact-text border w-100 p-4 d-flex justify-content-start align-items-center"
                                style="border-radius:40px;">
                                <h4>081384454401</h4>
                            </div>
                        </div>
                        <div class="container-contact   d-flex mt-25">
                            <img src="{{ asset('assets/frontend/images/icons/maps.svg') }}" alt=""
                                class="icons">
                            <div class="title-contact"><h5>Alamat</h5></div>
                            <div class="contact-text border w-100 p-4 d-flex justify-content-start align-items-center"
                                style="border-radius:40px;">
                                <h4>Jl. Babakan Sari No.135, Babakan Sari, Kec. Kiaracondong, Kota Bandung, Jawa Barat
                                    40283
                                </h4>
                            </div>
                        </div>
                        <p class="ajax-response"></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
