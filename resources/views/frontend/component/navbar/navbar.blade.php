    <!-- header start -->
    <header class="header">
        <div class="header__bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-9 col-lg-12">
                        <div class="navarea">
                            <a href="{{route('page.home')}}" class="site-logo">
                                <img src="{{asset('assets/frontend/images/logo/kiaracipta-logo.svg')}}" alt="LOGO">
                            </a>
                            <div class="mainmenu">
                                <nav id="mobile-menu">
                                    <ul>
                                        @php
                                        @endphp
                                        @foreach(Helper::MenuFrontPage() as $menu)
                                        <li><a href="{{$menu->url}}">{{ Helper::getLanguageJudul($menu->id) }}</a>
                                            @foreach (Helper::childFrontPage($menu->id) as $child)
                                            <ul class="sub-menu">
                                                <li><a href="{{$menu->url}}">{{ Helper::getLanguageJudul($child->id) }}</a></li>
                                            </ul>
                                            @endforeach
                                        </li>
                                        @endforeach
                                    </ul>
                                </nav>
                            </div>
                            <div class="mobile-menu"></div>
                        </div>
                    </div>
            </div>
        </div>
    </header>
    <!-- header end -->