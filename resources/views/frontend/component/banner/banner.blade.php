
<section class="hero owl-carousel">
@php
@endphp
@foreach (Helper::slider() as $slider)
            <div class="hero__item">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-xl-6 col-lg-7">
                        <div class="hero__content">
                            <h2 class="hero__title" data-animation="fadeIn" data-delay=".2s" data-duration=".5s">{{ $slider->join->judul }}</h2>
                            <p data-animation="fadeInUp" data-delay=".5s" data-duration=".7s">{{ $slider->join->deskripsi }}</p>
                            <button data-animation="fadeInUp" data-delay=".7s" data-duration=".9s" class="site-btn" onclick="openWhatsApp()">{{ $slider->title_button }}</button>
                            <div class="shape">
                                <img src="{{asset('assets/frontend/images/shape/hero-shape.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__image d-flex align-self-stretch">
                <img src="{{asset('assets/admin/assets/media/slider/'.$slider->image)}}" width="720" alt="">
            </div>
        </div>
@endforeach
    </section>
    <!-- hero end -->