    <!-- service section start -->
    <section class="service-area pt-125 pb-125" id="service">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-8 text-center">
                    <div class="section-header mb-75">
                        <h2 class="section-title">
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-1.png') }}"
                                    class="mr-5" alt=""></span>
                            Service
                            <span><img src="{{ asset('assets/frontend/images/shape/heading-shape-2.png') }}"
                                    class="mr-5" alt=""></span>
                        </h2>

                    </div>
                </div>
            </div>
            <div class="row mt-none-50">
                @foreach (Helper::service() as $service)
                    <div class="col-xl-6 col-lg-6 mt-50">
                        <div class="service-item d-flex">
                            <div class="service-item__icon service-item__icon--1">
                                <img src="{{ asset('assets/admin/assets/media/posting/' . $service->image) }}"
                                    alt="" style="width: 70px; height: 70px;">
                            </div>
                            <div class="service-item__content">
                                <h4 class="service-item__title">{{ $service->getPostingLanguage->judul }}</h4>
                                <p>{!! $service->getPostingLanguage->content !!}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-8 text-center">
                    <div class="view-all mt-55">
                        <p>Kami akan membantu mengangkat bisnis kecil Anda ke level berikutnya!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- service section end -->
