    <!-- footer start -->
    <footer class="footer pt-120">
        <div class="container">
            <div class="row mt-none-50 justify-content-center">
                <div class="col-xl-2 col-lg-3 mt-50">
                    <a href="{{route('page.home')}}" class="footer__logo">
                        <img src="{{asset('assets/frontend/images/logo/kiaracipta-logoWhite.svg')}}" alt="">
                    </a>
                </div>
                <div class="col-xl-2 col-lg-4 mt-50 pl-45 pr-0">
                    <div class="footer-widget">
                        <h4 class="widget-title">Service Kami</h4>
                        <ul>
                        @foreach (Helper::service() as $service )
                            <li><a href="#service"><i class="fa fa-angle-right"></i>{{ $service->getPostingLanguage->judul }}</a></li>
                        @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-5 mt-50 pl-70 pr-0">
                    <div class="footer-widget">
                        <h4 class="widget-title">Artikel Terbaru</h4>
                        <div class="recent-news mt-none-20">
                        @foreach (Helper::get_article_dashboard() as $article )
                            <div class="recent-news__content mt-20">
                                <a href="{{route('page.detail-showcase', $article->id)}}" class="recent-news__title">{{ $article->getPostingLanguage->judul }}</a>
                                <a href="{{route('page.detail-showcase', $article->id)}}" class="recent-news__date">{{ \Carbon\Carbon::parse($article->created_at)->locale('id')->isoFormat('dddd, D MMMM YYYY') }}</a>
                            </div>
                            <hr>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 offset-xl-1 col-lg-6 mt-50">
                    <div class="footer-widget">
                        <button class="order_sekarang d-flex" onclick="openWhatsApp()">Order Sekarang</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__bottom mt-115">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 my-auto">
                        <div class="copyright-text">
                            <p>Copyright &copy; <a href="https://themeforest.net/user/theme_pure">Theme Pure.</a> All Rights Reserved.</p>
                        </div>
                    </div>
                    {{-- <div class="col-lg-6">
                        <div class="social__links">
                            <a href="#0"><i class="fab fa-facebook-f"></i></a>
                            <a href="#0"><i class="fab fa-twitter"></i></a>
                            <a href="#0"><i class="fab fa-pinterest-p"></i></a>
                            <a href="#0"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->