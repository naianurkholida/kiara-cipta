<?php
return [
	'ourClients' => 'Klien Kami',
	'aboutUs' => 'Tentang Kami',
	'ourSupport' => 'Dukungan Kami',
	'faqs' => 'Faq',
	'services' => 'Produk & Layanan',
	'contact' => 'Kontak',
	'explore' => 'Telusuri',
	'recentPost' => 'Posting Terbaru',
	'relatedPost' => 'Posting Terkait'
];
?>