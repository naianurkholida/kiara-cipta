<?php
return [
	'ourClients' => 'Our Clients',
	'aboutUs' => 'About Us',
	'ourSupport' => 'Our Supports',
	'faqs' => 'Faqs',
	'services' => 'Services',
	'contact' => 'Contacts',
	'explore' => 'Explore',
	'recentPost' => 'Recent Post',
	'relatedPost' => 'Related Post'
];
?>