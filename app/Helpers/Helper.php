<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Session;
use App\Entities\Admin\core\Language;
use App\Entities\Admin\core\Category;
use App\Entities\Admin\core\Menu as Menu;
use App\Entities\Admin\core\MenuAccess as MenuAccess;
use App\Entities\Admin\core\MenuFrontPage;
use App\Entities\Admin\core\MenuFrontPageLanguage;
use App\Entities\Admin\core\Parameter;
use App\Entities\Admin\core\Sosmed;
use App\Entities\Admin\core\Slider;
use App\Entities\Admin\core\SlideLanguage;
use App\Entities\Admin\core\Treatment;
use App\Entities\Admin\core\TreatmentLanguage;
use App\Entities\Admin\core\Produk;
use App\Entities\Admin\core\Posting;
use App\Entities\Admin\core\PostingRelated;
use App\Entities\Admin\core\Pages;
use App\Entities\Admin\core\BestSellerIcon;
use App\Entities\Admin\core\OnlineStore;
use App\Entities\Admin\core\Gallery;
use DB;

class Helper
{
	public static function pathSlider(){
		$data = env('APP_URL').'/assets/admin/assets/media/slider';

		return $data;
	}

	public static function pathPosting()
	{
		$data = env('APP_URL').'/assets/admin/assets/media/posting/500/';

		return $data;
	}

	public static function pathGallery()
	{
		$data = env('APP_URL').'/assets/admin/assets/media/gallery/500/';

		return $data;
	}

	private static function baseLanguageId()
	{
		$data = Session::get('locale');

		return $data;
	}

	public static function baseLanguageName()
	{
		$data = Language::findOrFail(Session::get('locale'));

		return $data->code;
	}

	public static function language()
	{
		$data = language::all();

		return $data;
	}

	public static function baseInstagram()
	{
		$data = Parameter::where('key', 'instagram')->first();

		return $data->value;
	}

	public static function title()
	{
		$data = 'PT. FAJAR GEMINTANG NUSANTARA';

		return $data;
	}

	public static function logo()
	{
		$data = '/cp-fgn/images/ic.png';

		return $data;
	}

	public static function baseLabelPage()
	{
		$url = request()->url();
		$route = app('router')->getRoutes()->match(app('request')->create($url));
		$dataRoute = $route->action['as'];

		$data = Menu::where('url', $dataRoute)->first();

		if ($data) {
			$label = $data->label;

			session::put('label_page', $label);
		}
		
		return session::get('label_page');
	}

	public static function menus()
	{
		$data['menu_item'] = MenuAccess::select('*')
		->leftjoin('menus', 'menus.id', '=', 'menu_access.menu_id')
		->where('role_id', \Session::get('role_id'))
		->where('menus.parent_id', '0')
		->where('menus.deleted_at', null)
		->orderBy('order_num','ASC')
		->get();

		$data['setting']   = MenuAccess::select('*')
		->leftjoin('menus', 'menus.id', '=', 'menu_access.menu_id')
		->where('role_id', \Session::get('role_id'))
		->where('menus.parent_id', '!=', '0')
		->where('menus.deleted_at', null)
		->orderBy('order_num','ASC')
		->get();

		return $data;
	}
	
	public static function removeTags($tags)
	{
		$data = str_replace('&nbsp;','',substr(strip_tags($tags), 0, 140)) . '...';

		return $data;
	}

	public static function slider()
	{
		$data = Slider::with('join')->orderBy('order_num', 'ASC')->where('status', True)->inRandomOrder()->get();
		return $data;
	}

	public static function getCategory($id)
	{
		$data = Category::where('id_parent', $id)->get();

		return $data;
	}

	public static function MenuFrontPage()
	{
		$data = MenuFrontPage::where('id_sub_menu', 0)
				->where('is_active', 0)
				->orderBy('sort_order', 'ASC')
				->get();

		return $data;
	}

	public static function childFrontPage($id)
	{
		$data = MenuFrontPage::where('id_sub_menu', $id)->where('is_active', 0)->get();

		return $data;
	}

	public static function childFrontPageArray($id)
	{
		$data = MenuFrontPage::whereIn('id_sub_menu', $id)->where('is_active', 0)->get();
		return $data;
	}

	public static function getLanguageJudul($id)
	{
		$data = MenuFrontPageLanguage::where('id_menu_front_page', $id)->where('id_language', Session::get('locale'))->first();

		return $data->judul_menu;
	}

	public static function tanggal_indonesia($tgl, $tampil_hari=true){
		$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
		$nama_bulan = array (
			1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
			"September", "Oktober", "November", "Desember");
		$tahun=substr($tgl,0,4);
		$bulan=$nama_bulan[(int)substr($tgl,5,2)];
		$tanggal=substr($tgl,8,2);
		$text="";
		if ($tampil_hari) {
			$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));
			$hari=$nama_hari[$urutan_hari];
			$text .= $hari.", ";
		}
		$text .=$tanggal ." ". $bulan ." ". $tahun;
		return $text;
	}

	public static function posting()
	{
		$data = Posting::with('getPostingLanguage')
				->get();

		return $data;
	}
	public static function fitur_kami()
	{
		$data = Posting::with('getPostingLanguage')
		->where('id_category', 69)->get();

		return $data;
	}
	public static function portofolio_gallery_first(){
		$data = Posting::with('getPostingLanguage')->where('id_category', 70)->first();
		return $data;
	}
	public static function portofolio_gallery()
	{
		$data = Posting::with('getPostingLanguage')->where('id_category', 70)->skip(1)->take(4)->get();
		// dd($data);
		return $data;
	}
	public static function service()
	{
		$data = Posting::with('getPostingLanguage')->where('id_category', 71)->get();
		return $data;
	}
	public static function client()
	{
		$data = Posting::with('getPostingLanguage')->where('id_category', 72)->get();
		return $data;
	}

	#trimitra content
	public static function banner_terms()
	{
		$param = Parameter::where('key', 'banner-terms')->first()->value;

		$data = Pages::with('join')->where('id', $param)->first();

		return $data;
	}

	public static function reviews()
	{
		$data = Pages::with('join')->where('id_category', 68)->get();

		return $data;
	}

	public static function our_clients()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'our-clients');
		})->get();

		return $data;
	}

	public static function our_clients_recent()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'our-clients');
		})->orderby('created_at', 'desc')->limit(10)->get();

		return $data;
	}

	public static function our_clients_related_post($param)
	{
		$data = PostingRelated::with('posting', 'posting.getPostingLanguage')
		->whereHas('posting.category', function($q){
			$q->where('seo', 'services');
		})->where('id_posting', $param)->get();

		return $data;
	}

	public static function about_us()
	{
		$param = Parameter::where('key', 'about-us')->first()->value;

		$data = Pages::with('join')->where('id', $param)->first();

		return $data;
	}

	public static function services()
	{
		$data = Parameter::where('key', 'services-list')->first()->value;

		return explode(',', $data);
	}

	public static function services_view()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'services');
		})->orderby('created_at', 'asc')->get();

		return $data;
	}

	public static function services_related_post($param)
	{
		$data = PostingRelated::with('posting', 'posting.getPostingLanguage', 'posting.category')
		->whereHas('posting.category', function($q){
			$q->where('seo', 'services');
		})
		->where('id_posting', $param)->get();

		return $data;	
	}

	public static function services_recent()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'services');
		})->orderby('created_at', 'desc')->limit(10)->get();

		return $data;
	}

	public static function our_supports()
	{
		$data = Gallery::with('category')
				->whereHas('category', function($q){
					$q->where('seo', 'our-supports');
				})
				->where('deleted_at', null)
				->orderby('id', 'asc')->limit(6)->get();

		return $data;
	}

	public static function our_supports_view()
	{
		$data = Gallery::with('category')
				->whereHas('category', function($q){
					$q->where('seo', 'our-supports');
				})
				->where('deleted_at', null)
				->orderby('id', 'asc')->get();

		return $data;
	}

	public static function faqs()
	{
		$param = Parameter::where('key', 'faqs')->first()->value;
		$data = Pages::with('join')->where('id_category', $param)->get();

		return $data;
	}

	public static function portofolio()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'portofolio');
		})->limit(6)->get();

		return $data;
	}

	public static function portofolio_recent()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'portofolio');
		})->orderBy('created_at', 'desc')->limit(10)->get();

		return $data;
	}

	public static function portofolio_related_post($param)
	{
		$data = PostingRelated::with('posting', 'posting.getPostingLanguage')
		->whereHas('posting.category', function($q){
			$q->where('seo', 'portofolio');
		})->where('id_posting', $param)->get();

		return $data;
	}

	public static function portofolio_view()
	{
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('seo', 'portofolio');
		})->get();

		return $data;
	}

	public static function get_article() {
		$data = Posting::with('getPostingLanguage', 'category')
		->whereHas('category', function($q){
			$q->where('id', 73);
		})->get();
		// dd($data);

		return $data;
	}
	public static function get_article_dashboard(){
		$data = Posting::with('getPostingLanguage')->where('id_category', 73)->take(3)->latest()->get();
		return $data;
	}

	public static function contact_us()
	{
		$param = Parameter::where('key', 'contacts-us')->first()->value;

		$data = Pages::with('join')->where('id', $param)->first();

		return $data;
	}

	public static function footer_contact($param)
	{
		if($param == 'telepon'){
			$data = Parameter::where('key', 'telepon')->first()->value;
		}else if($param == 'phone'){
			$data = Parameter::where('key', 'phone')->first()->value;
		}else if($param == 'alamat'){
			$data = Parameter::where('key', 'alamat')->first()->value;
		}else if($param == 'alamat'){
			$data = Parameter::where('key', 'email')->first()->value;
		}else if($param == 'website'){
			$data = Parameter::where('key', 'website')->first()->value;
		}else if($param == 'twitter'){
			$data = Parameter::where('key', 'twitter')->first()->value;
		}else if($param == 'facebook'){
			$data = Parameter::where('key', 'facebook')->first()->value;
		}else if($param == 'instagram'){
			$data = Parameter::where('key', 'instagram')->first()->value;
		}else{
			$data = 'Tidak Ada Data';
		}

		return $data;
	}


}