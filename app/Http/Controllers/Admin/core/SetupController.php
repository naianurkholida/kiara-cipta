<?php

namespace App\Http\Controllers\Admin\core;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Entities\Admin\core\Menu;
use App\Entities\Admin\core\MenuAccess;
use App\Entities\Admin\core\Pages;
use App\Entities\Admin\core\SetupProfile;
use App\Entities\Admin\core\Sejarah;
use App\Entities\Admin\core\Kepengurusan;
use App\Entities\Admin\core\Parameter;
use App\Entities\Admin\core\Gambar;
use App\Entities\Admin\core\Category;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use Image;
use File;

class SetupController extends Controller
{
    public function __construct()
    {
        $this->path =  'assets/admin/assets/media/img';
    }

    public function top_bar()
    {
        $data['menu_item'] = MenuAccess::select('*')
        ->leftjoin('menus', 'menus.id', '=', 'menu_access.menu_id')
        ->where('role_id', \Session::get('role_id'))
        ->where('menus.parent_id', '0')
        ->where('menus.deleted_at', null)
        ->orderBy('order_num','ASC')
        ->get();

        $data['setting']   = MenuAccess::select('*')
        ->leftjoin('menus', 'menus.id', '=', 'menu_access.menu_id')
        ->where('role_id', \Session::get('role_id'))
        ->where('menus.parent_id', '!=', 0)
        ->where('menus.deleted_at', null)
        ->orderBy('order_num','ASC')
        ->get();

        return $data;
    }

    public function settings()
    {
        $top_bar = $this->top_bar();
        $pages = Pages::select('pages.id as key_page','pages_language.*')
        ->join('pages_language', 'pages_language.id_pages', '=', 'pages.id')
        ->where('pages_language.id_language', 1)
        ->get();

        $category= Category::where('id_parent', '=', 0)->get();

        $banner_terms = Parameter::where('key', 'banner-terms')->first()->value;
        $about_us = Parameter::where('key', 'about-us')->first()->value;
        $services = Parameter::where('key', 'services-list')->first()->value;
        $faqs = Parameter::where('key', 'faqs')->first()->value;
        $contacts_us = Parameter::where('key', 'contacts-us')->first()->value;

        $telepon = Parameter::where('key', 'telepon')->first()->value;
        $phone = Parameter::where('key', 'phone')->first()->value;
        $alamat = Parameter::where('key', 'alamat')->first()->value;
        $email = Parameter::where('key', 'email')->first()->value;
        $website = Parameter::where('key', 'website')->first()->value;

        return view('admin.core.setup.settings.index', 
            compact(
                'top_bar', 'pages', 'category', 'banner_terms', 'about_us', 'services', 'faqs', 'contacts_us',
                'telepon', 'phone', 'alamat', 'email', 'website'
            )
        );
    }

    public function store_settings(Request $request)
    {
        $banner_terms = Parameter::where('key', 'banner-terms')->first();
        $banner_terms->value = $request->banner_terms;
        $banner_terms->save();

        $about_us = Parameter::where('key', 'about-us')->first();
        $about_us->value = $request->about_us;
        $about_us->save();

        $services = Parameter::where('key', 'services-list')->first();
        $services->value = $request->services;
        $services->save();

        $faqs = Parameter::where('key', 'faqs')->first();
        $faqs->value = $request->faqs;
        $faqs->save();

        $contacts_us = Parameter::where('key', 'contacts-us')->first();
        $contacts_us->value = $request->contacts_us;
        $contacts_us->save();

        $telepon = Parameter::where('key', 'telepon')->first();
        $telepon->value = $request->telepon;
        $telepon->save();

        $phone = Parameter::where('key', 'phone')->first();
        $phone->value = $request->phone;
        $phone->save();

        $alamat = Parameter::where('key', 'alamat')->first();
        $alamat->value = $request->alamat;
        $alamat->save();

        $email = Parameter::where('key', 'email')->first();
        $email->value = $request->email;
        $email->save();

        $website = Parameter::where('key', 'website')->first();
        $website->value = $request->website;
        $website->save();

        return redirect()->back();
    }
}
