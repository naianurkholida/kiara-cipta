<?php

namespace App\Http\Controllers\FrontPage;

use App;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Entities\FrontPage\Inbox;
use App\Entities\Admin\core\Pages;
use App\Entities\Admin\core\Produk;
use App\Entities\Admin\core\Posting;
use App\Http\Controllers\Controller;
use App\Entities\Admin\core\Category;
use App\Entities\Admin\core\Language;
use App\Entities\Admin\core\Parameter;
use App\Entities\Admin\core\Treatment;
use App\Entities\FrontPage\Pengunjung;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use App\Entities\Admin\core\MenuFrontPage;
use App\Entities\Admin\core\MenuFrontPageLanguage;

class MainController extends Controller
{
    public function __construct(Request $request)
    {
        $this->view = 'fgn.index';

        $cekip = Pengunjung::where('ip', $request->ip())->first();

        if($cekip == null){
            $pengunjung = new Pengunjung;
            $pengunjung->ip = $request->ip();
            $pengunjung->save();
        }   
    }

    /**s
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        return view($this->view);
    }

    public function switch($language)
    {
        if($language == 'id'){
            $lang_id = 1;
        }else{
            $lang_id = 2;   
        }

        // Simpan locale ke session.
        Session::put('language', $language);
        Session::put('locale', $lang_id);
        App::setlocale($language);
        // Arahkan ke halaman sebelumnya.
        return redirect('/');
    }

    public function route(Request $request)
    {
        $url = str_replace(env('APP_URL').'/', '', $request->url());
        
        if($url == 'home'){
            return redirect('/');
        }

        return view('fgn._pages.'.$url);
    }

    public function posting(Request $request, $seo)
    {
        $data = Posting::with('getPostingLanguage', 'category')
        ->whereHas('category', function($q) use ($request){
            $q->where('seo', $request->segment(1));
        })
        ->whereHas('getPostingLanguage', function($q) use ($seo){
            $q->where('seo', $seo);
        })->first();

        if($request->segment(1) == 'our-clients'){
            $_page = 'posting';
        }else if($request->segment(1) == 'portofolio'){
            $_page = 'portofolio';
        }else if($request->segment(1) == 'services'){
            $_page = 'services';
        }else{
            abort(404);
        }

        return view('fgn._components.'.$_page, compact('data'));
    }

    public function gallery(Request $request)
    {
        return view('fgn._components.gallery');
    }

    public function postInbox(Request $request)
    {
        $inbox = new Inbox;
        $inbox->nama = $request->nama;
        $inbox->phone = $request->phone;
        $inbox->email = $request->email;
        $inbox->services = $request->services;
        $inbox->inbox = $request->message;
        $inbox->save();

        return response()->json(['status' => 200, 'message' => 'Message sent successfully']);
    }

    public function fgn()
    {
        return view('fgn.index');
    }
    public function kiara(){
        Session::put('locale', 1);
        return view('frontend.pages.home.home');
    }
    public function artikel(){
        return view('frontend.artikel.artikel');
    }
    public function detailArtikel(){
        return view('frontend.artikel.detail-artikel');
    }

}
