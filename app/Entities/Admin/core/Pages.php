<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Admin\core\PagesLanguage;
use App\Entities\Admin\core\Category;

class Pages extends Model
{
    protected $table = 'pages';

    public function join()
    {
    	return $this->hasOne(PagesLanguage::class, 'id_pages', 'id')->where('id_language', \Session::get('locale') ?? 1);
    }

    public function category()
    {
    	return $this->hasOne(Category::class, 'id_category', 'id');
    }
}
