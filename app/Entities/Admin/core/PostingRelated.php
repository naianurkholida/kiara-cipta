<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Admin\core\Posting;

class PostingRelated extends Model
{
    protected $table = 'posting_related';

    public function posting()
    {
    	return $this->hasOne(Posting::class, 'id', 'id_sub_posting');
    }
}
