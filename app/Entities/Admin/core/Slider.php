<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use App\Entities\Admin\core\SlideLanguage;

class Slider extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $table = 'slider';

    public function join()
    {
    	return $this->hasOne(SlideLanguage::class, 'slider_id', 'id');
    }
}
